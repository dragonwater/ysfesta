import 'package:flutter/material.dart';
import 'main.dart';
import 'apis.dart';


class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
  TextEditingController idController = TextEditingController();
  TextEditingController pwController = TextEditingController();
  int _cnt = 0;

  @override
  Widget build(BuildContext context) {
    DataStorage storage = new DataStorage();
    storage.readData().then((String value) {
      if(value != "" && value != null)
        if(_cnt++ == 0)
          Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => MyHomePage()));
    });
    final idField = TextField(
      obscureText: false,
      style: style,
      controller: idController,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "학번",
          border:
          OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );
    final passwordField = TextField(
      obscureText: true,
      style: style,
      controller: pwController,
      decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
          hintText: "비민번호",
          border:
          OutlineInputBorder(borderRadius: BorderRadius.circular(32.0))),
    );
    final loginButon = Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Colors.lime,
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () async {
          print("onPressed");
          String result = await send("LI|" + idController.text + "|" + pwController.text);
          print(result);
          if(result == "SI") {
            storage.writeData(idController.text.trim() + "|0");
            Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => MyHomePage()));
          }
          else {
            showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text("로그인 오류"),
                  content: Text("학번 또는 비밀번호가 잘못되었습니다."),
                  actions: <Widget>[
                    FlatButton(
                      child: Text("확인"),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    )
                  ],
                );
              }
          );
          }
        },
        child: Text("로그인",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );

    return Scaffold(
      body: Center(
        child: Container(
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.all(36.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: 155.0,
                  child: Text("안녕하세요.", style: TextStyle(fontSize: 50),)
                ),
                SizedBox(height: 45.0),
                idField,
                SizedBox(height: 25.0),
                passwordField,
                SizedBox(
                  height: 35.0,
                ),
                loginButon,
                SizedBox(
                  height: 15.0,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
