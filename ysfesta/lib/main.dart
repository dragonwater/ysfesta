import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'dart:async';
import 'dart:io';
import 'apis.dart';
import 'login.dart';

String address = "54.180.85.239";//"10.0.2.2";
int port = 5000;

String _id;
int _points = 0;


void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.green,
        ),
        home: LoginPage()
    );
  }
}

class MyHomePage extends StatefulWidget {

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  DataStorage storage;
  bool isConnecting = false;
  String barcode;
  int _cnt = 0;

  @override
  void initState() {
    storage = new DataStorage();
    _id = "";
    _points = 0;
    isConnecting = false;
    super.initState();
    storage.readData().then((String value) {
      if(value != "" && value != null) {
        List<String> l = value.split("|");
        setState(() {
          _id = l[0];
          _points = int.parse(l[1]);
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    /*
    if (_cnt++ == 0) {
      widget.storage.readData().then((String value) {
        if (value == "" || value == null) {
          Navigator.push(
              context, MaterialPageRoute<void>(builder: (BuildContext context) {
            return LoginPage();
          }));
        }
      });
    }*/
    /*widget.storage.readData().then((String value) {
      List<String> l = value.split("|");
      setState(() {
        _id = l[0];
        _points = int.parse(l[1]);
      });
    });*/
    return Scaffold(
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.refresh),
          onPressed: () {
            try {
              reloadPoint();
              storage.readData().then((String value) {
                print(value);
                if (value != "" && value != null) {
                  List<String> l = value.split("|");
                  setState(() {
                    _id = l[0];
                    _points = int.parse(l[1]);
                  });
                }
              });
            }
            catch(e) {
              print("catch error: $e");
            }
          }
      ),
      appBar: AppBar(
        title: Text("수향제 앱...!"),
      ),
      body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text("학생 계정"),
              Text(_id, style: TextStyle(fontSize: 40),),
              Padding(
                padding: const EdgeInsets.all(40.0),
                child: Container(),
              ),
              Text("잔고"),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(_points.toString(), style: TextStyle(fontSize: 80),),
                  Text("원"),
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Container(),
              ),
              RaisedButton(
                  color: Colors.blue,
                  textColor: Colors.white,
                  splashColor: Colors.blueGrey,
                  onPressed: () {
                    shotQR();
                    print("barcode is $barcode");
                  },
                  child: const Text("QR코드 찍기")
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Container(),
              )
            ],
          )
      ),
    );
  }

  Future<String> loadAsset(String path) async {
    return await rootBundle.loadString(path);
  }

  void saveDate(String id, int points) {
    //Do Something
  }


  Future<File> reloadPoint() async {
    if(isConnecting)
      return null;
    else
      isConnecting = true;

    String recvStr = await send("LD|" + _id);
    if(recvStr == "FL") {
      return null;
    }
    else {
      int newPoint = int.parse(recvStr.replaceFirst("SL|", ""));
      setState(() {
        _points = newPoint;
      });
      isConnecting = false;
      return storage.writeData(_id + "|" + newPoint.toString());
    }
  }

  void shotQR() async {
    print(_points);

    try {
      String barcode = await BarcodeScanner.scan();
      setState(() => this.barcode = barcode);
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        setState(() {
          this.barcode = 'The user did not grant the camera permission!';
        });
      } else {
        setState(() => this.barcode = 'Unknown error: $e');
      }
    } on FormatException{
      setState(() => this.barcode = 'null (User returned using the "back"-button before scanning anything. Result)');
    } catch (e) {
      setState(() => this.barcode = 'Unknown error: $e');
    }
    reloadPoint();
    storage.readData().then((String value) {
      if(value != "" && value != null) {
        List<String> l = value.split("|");
        setState(() {
          _id = l[0];
          _points = int.parse(l[1]);
        });
      }
    });/*
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(_points.toString()),
          content: Text(barcode),
          actions: <Widget>[
            FlatButton(
              child: Text("확인"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        );
      }
    );*/
    if(barcode.contains("PP")) {
      List<String> l = barcode.split("|");
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("Payment"),
              content: Text(l[2] + "원을 " + l[1] + "에 결제하시겠습니까?"),
              actions: <Widget>[
                FlatButton(
                  child: Text("아니오"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                FlatButton(
                  child: Text("예"),
                  onPressed: () {
                    if(_points > int.parse(l[2])) {
                      send(barcode + "|" + _id).then((String recvStr) {
                        print(recvStr);
                      });
                      Navigator.of(context).pop();

                      reloadPoint();
                      storage.readData().then((String value) {
                        if(value != "" && value != null) {
                          List<String> l = value.split("|");
                          setState(() {
                            _id = l[0];
                            _points = int.parse(l[1]);
                          });
                        }
                      });
                    }
                  },
                )
              ],
            );
          }
      );
    }
    else if(barcode.contains("AP")) {
      List<String> l = barcode.split("|");
      showDialog(
          context: context,
          builder: (BuildContext context) {
            send(barcode + "|" + _id).then((String recvStr) {
              
            });
            return AlertDialog(
              title: Text("Payment"),
              content: Text(l[2] + "원을 " + l[1] + "에 결제하시겠습니까?"),
              actions: <Widget>[
                FlatButton(
                  child: Text("아니오"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                FlatButton(
                  child: Text("예"),
                  onPressed: () {
                    if(_points > int.parse(l[2])) {
                      send(barcode + "|" + _id).then((String recvStr) {
                        print(recvStr);
                      });
                      Navigator.of(context).pop();

                      reloadPoint();
                      storage.readData().then((String value) {
                        if(value != "" && value != null) {
                          List<String> l = value.split("|");
                          setState(() {
                            _id = l[0];
                            _points = int.parse(l[1]);
                          });
                        }
                      });
                    }
                  },
                )
              ],
            );
          }
      );
    }
  }
}