import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'dart:convert';
import 'dart:async';


List getData(String str){
  List<String> strList = str.split('\n');
  String _id = strList[0].replaceFirst("ID:", "");
  int _points = int.parse(strList[1].replaceFirst("POINT:", ""));
  if (_id == "None" || (_id == "" || _id == null))
    return null;
  else {
    return [_id, _points];
  }
}

/*Future<String> sendData(String str) async {
  print("start connecting..");
  Completer<String> completer = new Completer<String>();
  String result = "NOT";
  Socket socket = await Socket.connect("54.180.85.239", 5000);
  socket.write(str);
  print("connected...");
  socket.listen((List<int> event) {
    String recvStr = utf8.decode(event);
    print("recv : $recvStr");
    if(str.contains("LD")) { //load data
      if(recvStr.contains("SL")) { //success loading
         result = recvStr.replaceFirst("SL|", "");
      }
    }
    else if(str.contains("PP")) { //pay point
      if(recvStr == "FP" || recvStr == "SP")
        result = recvStr;
      else
        result = "error in payment";
    }
    else if(str.contains("AP")) { //add point
      if(recvStr == "FA" || recvStr == "SA")
        result = recvStr;
      else
        result = "error in adding points";
    }
    completer.complete(result);
  },
  onError: () {

  },//cancel on error도 처리
  onDone: () {
    socket.destroy();
  });
  socket.close();
  return completer.future;
}*/
Future<String> send(String str) async {
  Completer<String> completer = new Completer<String>();
  Socket socket = await Socket.connect("54.180.85.239", 5000);
  socket.write(str);

  socket.listen((List<int> event) {
    String recvStr = utf8.decode(event);
    print(recvStr);
    completer.complete(recvStr);
  },
      onDone: () {
        socket.destroy();
      });
  socket.close();
  return completer.future;
}


class DataStorage {
  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/data.txt');
  }

  Future<String> readData() async {
    try {
      final file = await _localFile;

      // Read the file
      String contents = await file.readAsString();

      return contents;
    } catch (e) {
      // If encountering an error, return 0
      return "";
    }
  }

  Future<File> writeData(String data) async {
    final file = await _localFile;

    // Write the file
    return file.writeAsString('$data');
  }
}
